<?php

namespace App\Application\Actions;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Views\PhpRenderer;

final class HomeAction {
    private $renderer;

    public function __construct(PhpRenderer $renderer) {
        $this->renderer = $renderer;
    }

    /**
     * @throws \Throwable
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $asset = function (string $a): string {
            $manifestData = json_decode(file_get_contents('assets/manifest.json'), true);
            return $manifestData["assets/$a"] ?? '';
        };

        return $this->renderer->render($response, 'app.phtml', [
            'page'  => 'home',
            'asset' => $asset
        ]);
    }
}
