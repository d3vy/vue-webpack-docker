# Telepítés, indítás

 - `composer install`
 - `yarn` vagy `npm install`
 - `yarn build` vagy `npm run build`
 - `docker compose up`

# Vue

A fájlok az assets/vue mappában vannak, a mount a megfelelő nevű js fájlban történik az assets/js mappában.

# Webpack (Encore)

A webpack.config.js fájlban állíthatók be a mobulok/entrypoint-ok, és minden más is.
Itt a home és subpage entrypoint-okat hoztam létre, valamint egy style entrypoint-ot a közös css-nek.
A Webpack intézi a megfelelő fájlok elkészítését a megfelelő (dev/prod) környezetben a public/assets mappán belülre, illetve létrehoz egy entrypoints.json és egy manifest.json fájlt.

## entrypoints.json

Ez akkor kell, ha bekapcsoljuk a splitEntryChunks() vagy az enableSingleRuntimeChunk() funkciót a config-ban.
Előbbi optimalizáltan feldarabolja az elkészült entrypoint-okat, utóbbi létrehoz a közös funkciókkal egy runtime.js fájlt.
Ha használjuk bármelyiket, kell írnunk egy logikát az entrypoints.json fájl feldolgozására, hogy a megfelelő fájlokat húzzuk be a template-ben, ezért ezeket a funkciókat nem kapcsoltam be.

## manifest.json

Ez akkor kell, ha bekapcsoljuk az asset verziózást, ilyenkor az elkészült fájlok kapnak egy hash-t a fájlnévben, a manifest.json pedig gondoskodik róla, hogy az entrypoint-oknak megfelelő verziózott fájlt töltse be.
Ehhez írtam egy függvényt, amit tudunk használni (a slim Action-ökben használtam is, így meg tudtam hívni a templates/app.phtml fájlban):

```php
$asset = function (string $a): string {
    $manifestData = json_decode(file_get_contents('assets/manifest.json'), true);
    return $manifestData["assets/$a"] ?? '';
};
```

Pár példát még adtam statikus fájlok és képek áthelyezésére is, mert ugye a Webpack a fájlok frissítése előtt kitakarít a célmappában.

# Docker

Itt igazából csak a slim-et futtatom php-val jelenleg, így nem volt szükség semmi extra beállításra.

# Yarn / npm script-ek

 - "dev-server": "encore dev-server",
 - "dev": "encore dev",
 - "watch": "encore dev --watch",
 - "build": "encore production --progress"
